<?php

/*
 *  Built With Love In Hail, Saudi Arabia.
 *  Developer : Nasser Mohammed Al Anazi
 *  Contact: Mr.nasseralonazi@gmail.com
 */
header('Content-Type: text/html; charset=utf-8');
date_default_timezone_set('Etc/GMT-3');
session_start();
$time = $_SERVER['REQUEST_TIME'];
$timeout_duration = 3600;
if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY']) > $timeout_duration) {
    session_destroy();
    session_start();
}
$_SESSION['LAST_ACTIVITY'] = $time;

class _connect {

    public static function __connect() {
        $host = 'localhost';
        $user = 'root';
        $password = '';
        $database = 'haij.';
        $db = str_replace('.', '', $database);
        $connection = mysqli_connect($host, $user, $password, $db);
        if ($connection) {
            mysqli_query($connection, 'set character_set_client=\'utf8\'');
            mysqli_query($connection, 'set character_set_results=\'utf8\'');
            mysqli_query($connection, 'set collation_connection=\'utf8_general_ci\'');
            return array($connection, $database);
        } else {
            return false;
        }
    }

}

class _datebase {

    public static function _ctz_sql($sql) {
        $connection = _connect::__connect();
        $data = mysqli_query($connection[0], $sql);
        $result = mysqli_fetch_all($data);
        return $result;
    }

    public static function _get_as_objects($table, $conditions, $select = '*') {
        $connection = _connect::__connect();
        $conditions = ' where ' . $conditions;
        $sql = 'select ' . $select . ' from ' . $connection[1] . $table . $conditions;
        $data = mysqli_query($connection[0], $sql);
        $result = mysqli_fetch_assoc($data);
        return $result;
    }

    public static function _get_as_array($table, $conditions = '', $select = '*') {
        $connection = _connect::__connect();
        if ($conditions != '') {
            $conditions = ' where ' . $conditions;
        }
        $sql = 'select ' . $select . ' from ' . $connection[1] . $table . $conditions;
        $data = mysqli_query($connection[0], $sql);
        $result = mysqli_fetch_all($data);
        return $result;
    }

    public static function _insert($table, array $colmusAndValues) {
        $connection = _connect::__connect();
        foreach ($colmusAndValues as $singel_array) {
            if (empty($total_colmus)) {
                $total_colmus = $singel_array[0];
            } else {
                $total_colmus = $total_colmus . ',' . $singel_array[0];
            }
            if (is_string($singel_array[1])) {
                $value = '"' . $singel_array[1] . '"';
            } else {
                $value = $singel_array[1];
            }
            if (empty($total_valuse)) {
                $total_valuse = $value;
            } else {
                $total_valuse = $total_valuse . ',' . $value;
            }
        }
        $sql = 'insert into ' . $connection[1] . $table . '(' . $total_colmus . ') Values ' . '(' . $total_valuse . ')';
        echo $sql;
        $data = mysqli_query($connection[0], $sql);
        return $data;
    }

    public static function _update($table, array $colmusAndValues, $conditions) {
        $connection = _connect::__connect();
        if ($conditions != '') {
            $conditions = ' where ' . $conditions;
        }
        foreach ($colmusAndValues as $singel) {
            if (is_string($singel[1])) {
                $value = '"' . $singel[1] . '"';
            } else {
                $value = $singel[1];
            }
            if (empty($org)) {
                $org = $singel[0] . '=' . $value;
            } else {
                $org = $org . ',' . $singel[0] . '=' . $value;
            }
        }
        $sql = 'UPDATE ' . $connection[1] . $table . ' SET ' . $org .' '. $conditions;
        $data = mysqli_query($connection[0], $sql);
        return $data;
    }

    public static function _delete($table, $conditions) {
        $connection = _connect::__connect();
        $sql = 'delete from ' . $connection[1] . $table . ' where ' . $conditions;
        $data = mysqli_query($connection[0], $sql);
        return $data;
    }

}

class _security {

    public static function _post_filter($var_name) {
        $conn = _connect::__connect();
        return mysqli_real_escape_string($conn[0], filter_input(INPUT_POST, $var_name));
    }

    public static function _encryption($salt, $string) {
        $salt = '$6$rounds=5000$' . $salt;
        $output = sha1(md5(crypt($string, $salt)));
        return $output;
    }

    public static function _token_generator() {
        $token = _tools::_string_Generator(24);
        $_SESSION['token'] = $token;
        return $token;
    }

    public static function _token_check($token) {
        if (isset($_SESSION['token']) && $token == $_SESSION['token']) {
            unset($_SESSION['token']);
            return true;
        } else {
            return false;
        }
    }

}

class _tools {

    public static function _string_Generator($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function _uploading_image($temp) {
        $filename = $temp['name'];
        $filetemp = $temp['tmp_name'];
        $filesize = $temp['size'];
        $fileerror = $temp['error'];
        $fileExt = explode('.', $filename);
        $fileAct = strtolower(end($fileExt));
        $allowed = array('jpg', 'png', 'jpeg');
        if (in_array($fileAct, $allowed)) {
            if ($fileerror == 0 && $filesize < 4026225) {
                $filenamenew = uniqid('', TRUE) . ".$fileAct";
                $fileDes = $_SERVER['DOCUMENT_ROOT'] . '/images/uploads/' . $filenamenew;
                move_uploaded_file($filetemp, $fileDes);
                return $filenamenew;
            } else {
                return 'there was error';
            }
        } else {
            return 'unallowed file ext';
        }
    }

}

class _interface {

    public static function _input_Generator(array $labels, array $input_type_name_placeholder) {
        $inputs = '';
        for ($i = 0; count($input_type_name_placeholder) > $i; $i++) {
            $inputs = $inputs .
                    '<div class="form-group">
                        <label>' . $labels[$i] . '</label>
                        <input type="' . $input_type_name_placeholder[$i][0] . '" class="form-control" id="' . $input_type_name_placeholder[$i][1] . '" name="' . $input_type_name_placeholder[$i][1] . '" placeholder="ادخل ' . $labels[$i] . '">
                    </div>';
        }
        return $inputs;
    }

}

class _system {

    public static function _getWebName() {
        $name = _datebase::_get_as_objects('system', 'id=1');
        return $name['site_name'];
    }

    public static function _getgoals() {
        $goals = _datebase::_get_as_objects('system', 'id=1');
        return $goals['goals'];
    }

}
