<?php
/*
 *  Built With Love In Hail, Saudi Arabia.
 *  Developer : Nasser Mohammed Al Anazi
 *  Contact: Mr.nasseralonazi@gmail.com
 */

include $_SERVER['DOCUMENT_ROOT'] . '/_libs/_IO.php';

if (isset($_SESSION['user_id'])) {
    
} else {
    if (isset($_GET['login'])) {
        $username = $_POST['username'];
        $password = $_POST['password'];
        $check = _datebase::_get_as_objects('users', 'username="' . $username . '" and password="' . $password . '"');
        if ($check != NULL) {
            ?>
            <div class="alert alert-success" role="alert">
                تم تسجيل دخولك بنجاح وجاري اعادة توجيهك
            </div>
            <script>
                setInterval(function () {
                    $(location).attr("href", "/user/");
                }, 3000);
            </script>
            <?php
        } else {
            ?>
            <div class="alert alert-danger" role="alert">
                كلمة مرور/اسم مستخدم خاطئ
            </div>
            <?php
        }
    }
}



if (isset($_GET['user_info'])) {
    $user_id = $_POST['user_id'];
    if ($user_id != null) {
        $get_user = _datebase::_get_as_objects('users', 'passport_id="10000000000"');
        $to_json = json_encode($get_user);
        echo '' . $to_json . '';
    }
}

if (isset($_GET['update_location'])) {
    $user_id = $_POST['user_id'];
    $update_location = $_POST['update_location'];
    if ($user_id != NULL and $update_location != NULL) {
        _datebase::_update('users', array(array('current_location', $update_location)), 'passport_id="'.$user_id.'"');
    }
}