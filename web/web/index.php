<?php
include $_SERVER['DOCUMENT_ROOT'] . '/_libs/_IO.php';
/*
 *  Built With Love In Hail, Saudi Arabia.
 *  Developer : Nasser Mohammed Al Anazi
 *  Contact: Mr.nasseralonazi@gmail.com
 */
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>CMS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/style/css/animate.css" rel="stylesheet" type="text/css"/>
        <link href="/style/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <script src="/style/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="/style/js/popper.min.js" type="text/javascript"></script>
        <script src="/style/js/bootstrap.js" type="text/javascript"></script>
        <link href="https://fonts.googleapis.com/css?family=Galada" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Markazi+Text" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
        <style>
            html,body{
                width: 100%;
                direction: rtl;

            }
            body{
                width: 100%;
                height: 100%;
            }
            .header{
                padding-top: 1%;
                padding-bottom: 1%;
                width: 100%;
                height: 18%;
                z-index: 999;
                -moz-box-shadow: 1px 2px 12px rgba(0,0,0,.5);
                -webkit-box-shadow: 1px 2px 12px rgba(0,0,0,.5);
                box-shadow: 1px 2px 12px rgba(0,0,0,.5);
                position: relative;
            }
            .logo_2030{
                width: 20%;
            }
            .logo_haij{
                width: 13.5%;
            }
            .homeslide{
                width: 100%;
            }
            .map{
                width: 100%;
                height: 50%;
            }
            .footer{
                width: 100%;
                height: 5%;
                text-align: center;
                background: white;
                background-repeat: no-repeat;
                -moz-box-shadow: 1px 2px 12px rgba(0,0,0,.5);
                -webkit-box-shadow: 1px 2px 12px rgba(0,0,0,.5);
                box-shadow: 1px 2px 12px rgba(0,0,0,.5);
                z-index: 999;
            }
            .bar{
                width: 100%;
                height: 5%;
            }
            @media (max-width:800px){
                .header{
                    height: 15%;
                }
                .logo_2030{
                    width: 80%;
                }
                .logo_haij{
                    width: 60%;
                }
                .homeslide{
                    height: 50%;
                }
            }
        </style>
    </head>
    <body>
        <div class="bar animated fadeInDown">
            <div class="col">
                <div class="row">
                    <div class="col-2">

                    </div>
                    <div class=" col-4">
                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#reg">
                                    التسجيل
                                </button>
                            </div>
                            <div class="col">
                                <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#login">
                                    تسجيل الدخول
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class=" col-4">

                    </div>
                </div>
            </div>
        </div>
        <div class="header text-center animated fadeInDown">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <img src="/images/logo.jpg" class="img-fluid logo_2030" />
                    </div>
                    <div class="col">
                        <img src="/images/haij.png" class="img-fluid logo_haij" />
                    </div>
                </div>
            </div>
        </div>
        <div class="homeslide animated fadeIn">
            <div class="ool">
                <img src="/images/homeslide_ar.jpeg" class="img-fluid homeslide" />
            </div>
        </div>
        <div class="col">
            <div class="map" id="map">

            </div>
        </div>
        <div class="footer">
            جميع الحقوق محفوظة
        </div>
        <script>
            function myMap() {
                var mapProp = {
                    center: new google.maps.LatLng(51.508742, -0.120850),
                    zoom: 5,
                };
                var map = new google.maps.Map(document.getElementById("map"), mapProp);
            }
        </script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAA8NwgQCB45auMQCSnh0XGYUEGjEsQO8s&callback=myMap"></script>
        <div class="modal fade" id="login" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">تسجيل دخول</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form>
                        <div class="modal-body">
                            <div id="note"></div>
                                <?php
                                echo _interface::_input_Generator(array('اسم المستخدم', 'كلمة المرور'), array(array('text', 'username'), array('password', 'password')));
                                ?>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">أغلاق</button>
                                <button type="button" class="btn btn-primary" onclick="_login(username.value, password.value)">تسجيل دخول</button>
                            </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="reg" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">تسجيل دخول</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function _login($username, $password) {
                if ($username === '' || $password === '' ) {
                    $("#login_note").html('<div class="animated fadeIn alert alert-danger" id="note" role="alert" style="font-size: 80%;">يجب ملئ جميع الحقول</div>').one('animationend oAnimationEnd mozAnimationEnd webkitAnimationEnd', function () {
                        $('#note').removeClass('animated fadeIn');
                    });
                    ;
                } else {
                    $("#login_note").html('<div class="animated fadeIn alert alert-info" role="alert" style="font-size: 80%;">جاري تسجيل دخولك</div>');
                    $.post("/api/?login", {username: $username, password: $password}, function (result) {
                        $("#note").html(result);
                    });
            }
            }
        </script>
    </body>
</html>